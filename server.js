const express = require('express');
const app = express();
app.use(express.static("./public"));
app.set("view engine", "ejs");
const bodyParser = require('body-parser');
require('dotenv').config();
const port = process.env.PORT || 3000

app.use(express.static("./public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/* app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
}) */

let routes = require('./routes/route') //importing route
var http = require('http');
var server = http.createServer(app);
var io = require('socket.io').listen(server);
server.listen(port);
routes(app);

var activeUsers = [];

io.on('connection', socket => {
    socket.on('disconnect', () => {
        let roomChat = socket.Roomchat;
        if(activeUsers[roomChat]) {
            activeUsers[roomChat].splice(activeUsers[roomChat].indexOf(socket.Username), 1);
        io.sockets.in(roomChat).emit('update-list-users', activeUsers[roomChat]);
        }
    });
    socket.on('client-register', data => {
        let roomChat = data.roomchat;
        if (activeUsers[roomChat] && activeUsers[roomChat].indexOf(data.username) >= 0) {
            socket.emit('register-fail', data.username);
        } else {
            if(activeUsers[data.roomchat])  {
                activeUsers[data.roomchat].push(data.username);
            }   else {
                activeUsers[data.roomchat] = [];
                activeUsers[data.roomchat].push(data.username);
            }
            socket.Username = data.username;
            socket.Roomchat = data.roomchat;
            socket.join(data.roomchat);
            socket.emit('register-success', data);
            io.sockets.in(data.roomchat).emit('update-list-users', activeUsers[roomChat]);
        }
    });
    socket.on('client-logout', () => {
        let roomChat = socket.Roomchat;
        activeUsers[roomChat].splice(activeUsers[roomChat].indexOf(socket.Username), 1);
        socket.broadcast.emit('update-list-users', activeUsers[roomChat]);
    });
    socket.on('client-send-data', data => {
        io.to(socket.Roomchat).emit('server-send-data', { name: socket.Username, content: data });
    });
    socket.on('typing', data => {
        socket.broadcast.emit('typing', socket.Username);
    });
    socket.on('stop-typing', data => {
        socket.broadcast.emit('stop-typing', socket.Username);
    });
});

console.log('Socket Server started on: ' + port);